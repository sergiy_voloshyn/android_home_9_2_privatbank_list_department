package com.xyz.android_home_9_2;



        import android.telecom.Call;

        import com.google.gson.JsonElement;
        import com.google.gson.internal.ObjectConstructor;

        import java.util.List;

        import retrofit.Callback;
        import retrofit.RestAdapter;
        import retrofit.http.GET;
        import retrofit.http.Query;

/**
 * Created by user on 29.01.2018.
 */

public class Retrofit {
    private static final String ENDPOINT = "https://api.privatbank.ua/p24api";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        //https://api.privatbank.ua/p24api/pboffice?json&city=&address=
        @GET("/pboffice?json&city=&address=")
        void getBankData(Callback<List<DataSite>> callback);

    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getBankData(Callback<List<DataSite> >callback) {
        apiInterface.getBankData(callback);
    }

}



