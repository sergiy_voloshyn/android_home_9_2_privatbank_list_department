package com.xyz.android_home_9_2;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
/*

2 Дано: api приватБанка https://api.privatbank.ua/#p24/branch Необходимо сделать приложение,
которое будет выдавать список отделений банка (название, город, адрес, телефон).
Данные также необходимо кешировать при первом запуске. Далее - использовать локальный кеш.
2.1* Добавить возможность обновления закешированых данных, но не чаще, чем раз в сутки.
2.2* Добавить фильтрацию по названию отделения и городу.


 {
    "name": "Отделение \"ЦПОИК в г.Александрия\" ПриватБанка",
    "state": "Кировоградская",
    "id": "2516",
    "country": "Украина",
    "city": "Кировоград",
    "index": "28000",
    "phone": "(8-05235) 4-12-47",
    "email": "Elena.Muntjanova@pbank.com.ua",
    "address": "ул Калинина 30"
  },
 */

public class MainActivity extends AppCompatActivity {

    ListAdapter listAdapter;
    @BindView(R.id.list)
    RecyclerView listView;

    @BindView(R.id.editTextFind)
    EditText editTextFind;

    List<DataSite> dataFromSite;
    List<DataSite> dataFromSiteTemp;

    public static final String daySaved = "DAY_OF_YEAR";
    public static final String settings = "settings";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        boolean updateFlag = false;
        boolean fileFound = true;


        Calendar calendar = Calendar.getInstance();
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);



        SharedPreferences sp = getSharedPreferences(settings, Context.MODE_PRIVATE);
        int dayCount = sp.getInt(daySaved, 0);


        if (dayCount == 0) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt(daySaved, dayOfYear);
            editor.apply();
            updateFlag = true;

        } else {
            if (dayOfYear != dayCount ) {
                updateFlag = true;

            }
        }
        if (!updateFlag) {

            FileInputStream fis = null;
            ObjectInputStream inputStream = null;


            try {
                fis = openFileInput("info.json");
                inputStream = new ObjectInputStream(fis);
                dataFromSite = (List<DataSite>) inputStream.readObject();
                dataFromSiteTemp = dataFromSite;


            } catch (Exception ex) {
                ex.printStackTrace();
                fileFound = false;


            } finally {
                try {
                    if (fis != null) {
                        fis.close();
                    }
                    if (inputStream != null) inputStream.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    fileFound = false;
                }
            }

            if (fileFound) {
                listAdapter = new ListAdapter(dataFromSite, MainActivity.this);
                LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                listView.setLayoutManager(layoutManager);
                listView.setAdapter(listAdapter);
            }

        }
        if (updateFlag || !fileFound) {
            //if (!fileFound)
            {


                Retrofit.getBankData(new Callback<List<DataSite>>() {
                    @Override
                    public void success(List<DataSite> dataSite, Response response) {

                        FileOutputStream fos = null;
                        ObjectOutputStream outputStream = null;
                        try {
                            dataFromSite = dataSite;
                            dataFromSiteTemp = dataFromSite;
                            fos = openFileOutput("info.json", Context.MODE_PRIVATE);
                            outputStream = new ObjectOutputStream(fos);
                            outputStream.writeObject(dataFromSite);

                            listAdapter = new ListAdapter(dataFromSite, MainActivity.this);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                            listView.setLayoutManager(layoutManager);
                            listView.setAdapter(listAdapter);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("Alert")
                                    .setMessage(ex.toString())
                                    .setCancelable(true)
                                    .show();
                        } finally {
                            try {
                                if (fos != null) fos.close();
                                if (outputStream != null) outputStream.close();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }


                    @Override
                    public void failure(RetrofitError error) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Alert")
                                .setMessage(error.toString())
                                .setCancelable(true)
                                .show();
                    }
                });
            }
        }

    }

    @OnClick(R.id.buttonStart)
    public void startButton() {


        List<DataSite> dataCity = dataFromSite;


        HashSet<DataSite> newSet = new HashSet<DataSite>();

        String strToFind = editTextFind.getText().toString().toLowerCase();

        for (int i = 0; i < dataCity.size(); i++) {
            String temp = dataCity.get(i).city.toLowerCase();
            if (temp.equals(strToFind)) newSet.add(dataCity.get(i));
        }
        if (strToFind.equals("")) {
            dataFromSite = dataFromSiteTemp;
        } else {

            dataFromSite = new ArrayList<DataSite>(newSet);
        }


        listAdapter = new ListAdapter(dataFromSite, MainActivity.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(listAdapter);


    }
}



