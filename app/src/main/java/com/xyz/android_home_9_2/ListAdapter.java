package com.xyz.android_home_9_2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {


    List<DataSite> list;
    Context context;


    public ListAdapter(List<DataSite> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.updateData(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        //(название, город, адрес, телефон
        @BindView(R.id.name_bank)
        TextView title_bank;

        @BindView(R.id.phone_bank)
        TextView phone_bank;

        @BindView(R.id.city_bank)
        TextView city_bank;

        @BindView(R.id.city_address)
        TextView address_bank;


        DataSite data;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void updateData(DataSite data) {
            this.data = data;

            title_bank.setText(data.name);
            phone_bank.setText(data.phone);
            city_bank.setText(data.city);
            address_bank.setText(data.address);


        }

    }
}