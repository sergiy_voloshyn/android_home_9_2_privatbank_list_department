package com.xyz.android_home_9_2;

import java.io.Serializable;
import java.util.Arrays;

public class DataSite implements Serializable {
    public String name;
    public String state;
    public String id;
    public String country;
    public String city;
    public String index;
    public String phone;
    public String email;
    public String address;


    @Override
    public String toString() {
        return "DataSite{" +
                "name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", id='" + id + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", index='" + index + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}


